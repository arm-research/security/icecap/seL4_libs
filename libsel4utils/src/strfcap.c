/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 SRI International
 *
 * This software was developed by SRI International and the University of
 * Cambridge Computer Laboratory (Department of Computer Science and
 * Technology) under DARPA contract HR0011-18-C-0016 ("ECATS"), as part of the
 * DARPA SSITH research programme.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

// #include <sys/cdefs.h>
// #include <sys/param.h>
// 

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sel4utils/util.h>


/*
 * Programmer-friendly macros for CHERI-aware C code -- requires use of
 * CHERI-aware Clang/LLVM, and full capability context switching.
 */
#define	cheri_getlen(x)		__builtin_cheri_length_get((x))
#define	cheri_getlength(x)	__builtin_cheri_length_get((x))
#define	cheri_getbase(x)	__builtin_cheri_base_get((x))
#define	cheri_getoffset(x)	__builtin_cheri_offset_get((x))
#define	cheri_getaddress(x)	__builtin_cheri_address_get((x))
#define	cheri_getflags(x)	__builtin_cheri_flags_get((x))
#define	cheri_getperm(x)	__builtin_cheri_perms_get((x))
#define	cheri_getsealed(x)	__builtin_cheri_sealed_get((x))
#define	cheri_gettag(x)		__builtin_cheri_tag_get((x))
#define	cheri_gettype(x)	((long)__builtin_cheri_type_get((x)))


/*
 * Soft implementation of cheri_subset_test().
 * Test whether a capability is a subset of another.
 * NOTE: This is to be replaced by LLVM intrinsic once the intrinsic and
 * related instruction arguments are stable.
 */
#define	cheri_is_subset(parent, ptr)					\
	(cheri_gettag(parent) == cheri_gettag(ptr) &&			\
	 cheri_getbase(ptr) >= cheri_getbase(parent) &&			\
	 cheri_gettop(ptr) <= cheri_gettop(parent) &&			\
	 (cheri_getperm(ptr) & cheri_getperm(parent)) == cheri_getperm(ptr))

#define	cheri_is_null_derived(x)					\
	__builtin_cheri_equal_exact((uintcap_t)cheri_getaddress(x), x)

/* Create an untagged capability from an integer */
#define cheri_fromint(x)	cheri_incoffset(NULL, x)

/* Increment @p dst to have the address of @p src */
#define cheri_copyaddress(dst, src)	(cheri_setaddress(dst, cheri_getaddress(src)))

/* Get the top of a capability (i.e. one byte past the last accessible one) */
#define	cheri_gettop(cap)	__extension__({			\
	__typeof__(cap) c = (cap);				\
	(cheri_getbase(c) + cheri_getlen(c));			\
})

/* Reserved CHERI object types: */
#define	CHERI_OTYPE_UNSEALED	(0l)
#define	CHERI_OTYPE_SENTRY	(1l)

/*
 * CHERI ISA-defined constants for capabilities -- suitable for inclusion from
 * assembly source code.
 */
#define	CHERI_PERM_GLOBAL			(1 << 0)	/* 0x00000001 */
#define	CHERI_PERM_EXECUTIVE			(1 << 1)	/* 0x00000002 */
#define	CHERI_PERM_SW0				(1 << 2)	/* 0x00000004 */
#define	CHERI_PERM_SW1				(1 << 3)	/* 0x00000008 */
#define	CHERI_PERM_SW2				(1 << 4)	/* 0x00000010 */
#define	CHERI_PERM_SW3				(1 << 5)	/* 0x00000020 */
#define	CHERI_PERM_MUTABLE_LOAD			(1 << 6)	/* 0x00000040 */
#define	CHERI_PERM_COMPARTMENT_ID		(1 << 7)	/* 0x00000080 */
#define	CHERI_PERM_BRANCH_SEALED_PAIR		(1 << 8)	/* 0x00000100 */
#define	CHERI_PERM_CCALL			CHERI_PERM_BRANCH_SEALED_PAIR
#define	CHERI_PERM_SYSTEM			(1 << 9)	/* 0x00000200 */
#define	CHERI_PERM_SYSTEM_REGS			CHERI_PERM_SYSTEM
#define	CHERI_PERM_UNSEAL			(1 << 10)	/* 0x00000400 */
#define	CHERI_PERM_SEAL				(1 << 11)	/* 0x00000800 */
#define	CHERI_PERM_STORE_LOCAL_CAP		(1 << 12)	/* 0x00001000 */
#define	CHERI_PERM_STORE_CAP			(1 << 13)	/* 0x00002000 */
#define	CHERI_PERM_LOAD_CAP			(1 << 14)	/* 0x00004000 */
#define	CHERI_PERM_EXECUTE			(1 << 15)	/* 0x00008000 */
#define	CHERI_PERM_STORE			(1 << 16)	/* 0x00010000 */
#define	CHERI_PERM_LOAD				(1 << 17)	/* 0x00020000 */
static ssize_t
_strfcap(char * __restrict buf, size_t maxsize, const char * __restrict format,
    void * __capability cap, bool tag)
{
	char tmp[(sizeof(void * __capability) * 2) + 1], fmt[9], *fmtp;
	const char *percent, *opt_start = NULL;
	char number_fmt, *orig_buf;
	size_t size = 0;
	long number;
	int width = 1, precision = 1;
	bool alt = false, right_pad = false;

#define	_OUT(str, len)							\
	do {								\
		if (size < maxsize)					\
			memcpy(buf, (str), MIN((len), maxsize - size));	\
		buf += (len);						\
		size += (len);						\
	} while(0)

#define	FLUSH_OPT()							\
	do {								\
		if (opt_start != NULL) {				\
			_OUT(opt_start, percent - opt_start);		\
			opt_start = NULL;				\
		}							\
	} while (0)

#define OUT(str)							\
	do {								\
		FLUSH_OPT();						\
		_OUT((str), strlen(str));				\
	} while (0)

	orig_buf = buf;

	for (; *format; ++format) {
		if (*format != '%') {
			if (size < maxsize)
				*buf = *format;
			buf++;
			size++;
			continue;
		}

		number_fmt = 'd';
		percent = format;
more_spec:
		switch (*++format) {
		case '\0':
			*orig_buf = '\0';
			return (-1);
			continue;

		case '1': case '2': case '3': case '4': case '5':
		case '6': case '7': case '8': case '9':
			width = *format - '0';
			while (*++format >= '0' && *format <= '9')
				width = (width * 10) + *format - '0';
			--format;
			goto more_spec;

		case '.':
			precision = 0;
			while (*++format >= '0' && *format <= '9')
				precision = (precision * 10) + *format - '0';
			--format;
			goto more_spec;

		case '-':
			right_pad = true;
			goto more_spec;

		case 'a':
			number = cheri_getaddress(cap);
			break;

		case 'A':
			if (!tag || cheri_getsealed(cap)) {
				OUT("(");
				if (!tag) {
					OUT("invalid");
					if (cheri_getsealed(cap))
						OUT(",");
				}
				switch cheri_gettype(cap) {
				case CHERI_OTYPE_UNSEALED:
					break;
				case CHERI_OTYPE_SENTRY:
					OUT("sentry");
					break;
				default:
					OUT("sealed");
					break;
				}
				OUT(")");
			} else
				opt_start = NULL;
			continue;

		case 'b':
			number = cheri_getbase(cap);
			break;

		case 'B':
			FLUSH_OPT();
			for (char *bytes = (char *)&cap;
			    bytes < (char *)&cap + sizeof(cap); bytes++) {
				snprintf(tmp, sizeof(tmp), "%02hhx", *bytes);
				_OUT(tmp, 2);
			}
			continue;

		case 'C': {
			size_t ret;

			FLUSH_OPT();
			if (cheri_is_null_derived(cap)) {
				alt = true;
				number_fmt = 'x';
				number = cheri_getaddress(cap);
				break;
			}
			ret = _strfcap(buf,
			    maxsize > size ? maxsize - size : 0,
			    "%#xa [%P,%#xb-%#xt]%? %A", cap, tag);
			buf += ret;
			size += ret;
			continue;
		}

		case 'l':
			number = cheri_getlength(cap);
			break;

		case 'o':
			number = cheri_getoffset(cap);
			break;

		case 'p':
			number = cheri_getperm(cap);
			break;

		case 'P':
			if (cheri_getperm(cap) & CHERI_PERM_LOAD)
				OUT("r");
			if (cheri_getperm(cap) & CHERI_PERM_STORE)
				OUT("w");
			if (cheri_getperm(cap) & CHERI_PERM_EXECUTE)
				OUT("x");
			if (cheri_getperm(cap) & CHERI_PERM_LOAD_CAP)
				OUT("R");
			if (cheri_getperm(cap) & CHERI_PERM_STORE_CAP)
				OUT("W");
			if (cheri_getperm(cap) & CHERI_PERM_SEAL)
				OUT("S");
#ifdef CHERI_PERM_EXECUTIVE
			if (cheri_getperm(cap) & CHERI_PERM_EXECUTIVE)
				OUT("E");
#endif
			continue;

		case 's':
			number = cheri_gettype(cap);
			break;

		case 'S':
			switch cheri_gettype(cap) {
			case CHERI_OTYPE_UNSEALED:
				OUT("<unsealed>");
				continue;
			case CHERI_OTYPE_SENTRY:
				OUT("<sentry>");
				continue;
			}
			number = cheri_gettype(cap);
			break;

		case 't':
			number = cheri_gettop(cap);
			break;

		case 'T':
			tag = true;
			continue;

		case 'v':
			number = cheri_gettag(cap);
			break;

		case 'x':
		case 'X':
			number_fmt = *format;
			goto more_spec;

		case '?':
			opt_start = format + 1;
			while(*(format + 1) != '\0' && *(format + 1) != '%')
				format++;
			if (opt_start == format + 1)
				opt_start = NULL;	/* or error? */
			continue;

		case '%':
			OUT("%");
			continue;

		case '#':
			alt = true;
			goto more_spec;
		}

		/* If we're here, we're rendering a number. */
		fmtp = fmt;
		*fmtp++ = '%';
		if (alt)
			*fmtp++ = '#';
		if (right_pad)
			*fmtp++ = '-';
		*fmtp++ = '*';
		*fmtp++ = '.';
		*fmtp++ = '*';
		*fmtp++ = 'l';
		*fmtp++ = number_fmt;
		*fmtp = '\0';
		snprintf(tmp, sizeof(tmp), fmt, width, precision, number);
		OUT(tmp);
	}

	orig_buf[MIN(size, maxsize - 1)] = '\0';
	return (size);
}

ssize_t
strfcap(char * __restrict buf, size_t maxsize, const char * __restrict format,
	void * __capability cap)
{
	return (_strfcap(buf, maxsize, format, cap, cheri_gettag(cap)));
}
